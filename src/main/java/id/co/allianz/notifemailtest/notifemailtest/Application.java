package id.co.allianz.notifemailtest.notifemailtest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Application implements CommandLineRunner {

	private final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {

		String eventId = "eventId";

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		headers.add("X_NOTIF_TOKEN", "6873dc0f-cab8-465f-b3dc-0fcab8965f25"); // immediate
		headers.add("Bearer ",
				"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJrYWZrYS1jb25uZWN0Iiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sImV4cCI6MTU4NzU1MzYzNCwiaWF0IjoxNTg3NTUwMDM0LCJhdXRob3JpdGllcyI6WyJST0xFX1JFQUQiLCJST0xFX1dSSVRFIl0sImp0aSI6IjJlMDI3MTMzLTkxZjktNGQxMS1hZjAwLWM5NWY1YTkzMmIzNCIsImNsaWVudF9pZCI6Im5vdGlmaWNhdGlvbl9zZXJ2aWNlIn0.i0Qfzw7gEiPqgQwIV_DpXgWGFt2LVndsKwSHEth5XAQ");

		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		map.add("to", "irfan.romadona@allianz.co.id");
		map.add("cc", "irfan.romadona@allianz.co.id");
		map.add("subject", "Kafka Connect Unresponsiveness Detected");
		map.add("from", "sauron-no-reply@allianz.co.id");
		map.add("body", "Last Attempt happened at: " + eventId);

		final HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, headers);
		ResponseEntity<String> response = null;

		try {
			response = restTemplate.postForEntity(
					"http://idazl0008.srv.allianz/gateway/send-email/api/v3/single/plain/emails", request,
					String.class);
		} catch (org.springframework.web.client.HttpClientErrorException hcee) {

			log.error("Error sending email: {}", hcee.getMessage());

		}

		if (response != null) {
			log.info("Notification Service Response: {} ", response.getBody());
			log.info("EMAIL SEND     ( {} )", eventId);
		}

	}

}
